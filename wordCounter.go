package main

import (
	"bitbucket.org/xvontas/word-counter/heapSort"
	"bitbucket.org/xvontas/word-counter/radix"
	"bitbucket.org/xvontas/word-counter/tokenizer"
	"bitbucket.org/xvontas/word-counter/types"

	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	var topN = flag.Int("n", 20, "top N results")

	flag.Parse()
	args := flag.Args()

	if *topN < 1 {
		exitWithError(errors.New("n parameter must be greater than 0"))
	}
	if len(args) == 0 {
		exitWithError(errors.New("specify an input file"))
	}
	fileName := args[0]

	b, err := ioutil.ReadFile(fileName)
	if err != nil {
		exitWithError(err)
	}

	r := radix.NewTree()
	for _, w := range tokenizer.Parse(b) {
		r.Insert(w)
	}

	var entries []*types.Entry
	r.Walk(func(n *radix.Node) {
		entries = append(entries, n.Entry)
	})

	heapSort.Sort(entries, func(a, b *types.Entry) bool {
		return a.Count() > b.Count()
	})

	for _, e := range entries[:*topN] {
		fmt.Println(e.Count(), string(e.Word()))
	}
}

func exitWithError(e error) {
	fmt.Fprintln(os.Stderr, e)
	os.Exit(1)
}
