package main

import (
	"bitbucket.org/xvontas/word-counter/radix"
	"bitbucket.org/xvontas/word-counter/tokenizer"
	"io/ioutil"
	"sort"
	"testing"
)

var bashResult = map[string]int{
	"the":  4284,
	"and":  2192,
	"of":   2185,
	"a":    1861,
	"to":   1685,
	"in":   1366,
	"i":    1056,
	"that": 1024,
	"his":  889,
	"it":   821,
	"he":   783,
	"but":  616,
	"was":  603,
	"with": 595,
	"s":    577,
	"is":   564,
	"for":  551,
	"all":  542,
	"as":   541,
	"at":   458,
}

func TestE2e(t *testing.T) {
	b, err := ioutil.ReadFile("./data/mobydick.txt")
	if err != nil {
		exitWithError(err)
	}

	r := radix.NewTree()
	for _, w := range tokenizer.Parse(b) {
		r.Insert(w)
	}

	type entry struct {
		word  []byte
		count int
	}

	var entries []entry
	r.Walk(func(n *radix.Node) {
		entries = append(entries, entry{n.Word(), n.Count()})
	})

	sort.Slice(entries, func(i, j int) bool {
		return entries[i].count > entries[j].count
	})

	for _, e := range entries[:len(bashResult)] {
		w := string(e.word)
		refCount := bashResult[w]
		if refCount != e.count {
			t.Errorf("%s: \texpected: %d, actual: %d\n", w, refCount, e.count)
		}
	}
}
