package types

type Entry struct {
	word  []byte
	count int
}

func NewEntry(c int, w []byte) *Entry {
	return &Entry{w, c}
}

func (e *Entry) Word() []byte {
	return e.word
}

func (e *Entry) Count() int {
	return e.count
}

func (e *Entry) Increment() {
	e.count++
}

func (e *Entry) Update(c int, w []byte) {
	e.count = c
	e.word = w
}
