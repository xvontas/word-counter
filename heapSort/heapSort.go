package heapSort

import "bitbucket.org/xvontas/word-counter/types"

var sfn func(a, b *types.Entry) bool

func Sort(input []*types.Entry, lessFn func(a, b *types.Entry) bool) {
	if len(input) == 0 {
		return
	}
	first := 0
	low := 0
	high := len(input)
	sfn = lessFn

	// heapify structure
	for i := (high - 1) / 2; i >= 0; i-- {
		siftNodes(input, i, high, first)
	}

	// remove top
	for i := high - 1; i >= 0; i-- {
		swap(input, first, first+i)
		siftNodes(input, low, i, first)
	}

}

func swap(data []*types.Entry, a, b int) {
	t := data[a]
	data[a] = data[b]
	data[b] = t
}

func siftNodes(data []*types.Entry, low, high, first int) {
	root := low
	for {
		child := 2*root + 1
		if child >= high {
			break
		}
		left := data[first+child]
		right := data[first+child+1]
		if child+1 < high && sfn(left, right) {
			child++
		}
		if !sfn(data[first+root], left) {
			return
		}

		swap(data, first+root, first+child)
		root = child
	}
}
