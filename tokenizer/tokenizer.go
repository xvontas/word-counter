package tokenizer

func Parse(input []byte) [][]byte {
	var words [][]byte
	var word []byte
	for _, t := range input {
		resetWord := func() {
			if len(word) == 0 {
				return
			}
			words = append(words, word)
			word = nil
		}

		switch t {
		case 10: // line change
			resetWord()
			break
		case 32: // space
			resetWord()
			break
		default:
			if t >= 65 && t <= 90 {
				word = append(word, t+32)
			} else if t >= 97 && t <= 122 {
				word = append(word, t)
			} else {
				resetWord()
			}
			break
		}
	}
	return words
}
