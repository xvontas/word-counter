To run:

`go wordCounter.go -n 20 data/mobydick.txt`

## Todo (after ~6h):

- fix bug with comparisons in radix tree (e.g. 's' appears multiple times)
- implement heap like data structure to sort frequencies instead of using `sort.Slice`
