package radix

import (
	"testing"
)

func TestMatchingPrefix(t *testing.T) {
	tests := []struct {
		l string
		r string
		o int
	}{
		{l: "", r: "", o: -1},
		{l: "", r: "a", o: -1},
		{l: "a", r: "a", o: 0},
		{l: "ab", r: "abc", o: 1},
	}

	for k, test := range tests {
		output := matchingPrefix([]byte(test.l), []byte(test.r))
		if output != test.o {
			t.Errorf("%d: failed!", k)
		}
	}
}

func TestTree(t *testing.T) {
	nt := NewTree()
	words := []string{
		"and",
		"are",
		"allowable",
		"s",
		"suppose",
		"supreme",
		"the",
		"this",
		"the",
		"the",
		"tech",
		"tall",
		"till",
		"touching",
		"to",
		"too",
		"to",
		"harpooning",
		"heeling",
		"hurrah",
		"hurried",
		"hinting",
		"handedly",
		"humanities",
		"height",
		"heaving",
		"howled",
		"hairs",
		"h",
		"he",
	}
	for _, w := range words {
		nt.Insert([]byte(w))
	}

	nt.Walk(func(i *Node) {
		if i == nil {
			t.Log("nil found!")
			return
		}
		t.Log(string(i.Word()), i.Count())
	})
}
