package radix

import "bitbucket.org/xvontas/word-counter/types"

type Node struct {
	edges []*edge
	*types.Entry
}

func newNode(count int, word []byte, edges []*edge) *Node {
	return &Node{edges, types.NewEntry(count, word)}
}

func (n *Node) isRoot() bool {
	return n.Entry == nil
}

func (n *Node) isLeaf() bool {
	return len(n.edges) == 0
}

func (n *Node) Edges() []*edge {
	return n.edges
}

type edge struct {
	target *Node
	label  []byte
}

type tree struct {
	root *Node
}

func NewTree() tree {
	return tree{&Node{}}
}

func (t *tree) Insert(word []byte) {
	if len(word) == 0 {
		return
	}

	n := t.root
	fragment := word

outer_loop:
	for n != nil {
		if n.isLeaf() {
			n.edges = append(n.edges,
				&edge{
					target: newNode(1, word, nil),
					label:  fragment})
			break
		}

		for i, currentEdge := range n.edges {
			matchingUntil := matchingPrefix(currentEdge.label, fragment)

			if matchingUntil == -1 {
				continue
			}

			ll := len(currentEdge.label)
			matchEdge := matchingUntil + 1

			// check is an exact match
			if len(fragment) == ll && matchEdge == ll {
				n.edges[i].target.Update(n.edges[i].target.Count()+1, word)
				break outer_loop
			}

			if matchEdge == ll {
				fragment = fragment[matchEdge:]
				n = n.edges[i].target
				// continue to next Node
				continue outer_loop
			}

			// fragment is shorter than the current label no need to go deeper

			oldEdge := &edge{
				target: currentEdge.target,
				label:  currentEdge.label[matchEdge:]}

			newWord := currentEdge.label[:matchEdge]
			if len(fragment) == 1 {
				newWord = word
			}

			n.edges[i] = &edge{
				target: newNode(0, newWord, []*edge{oldEdge}),
				label:  currentEdge.label[:matchEdge]}

			if len(fragment) > 1 {
				fragment = fragment[matchEdge:]
				n = n.edges[i].target
			}
			// continue to next Node
			continue outer_loop
		}

		// this prefix has not been seen before
		n.edges = append(n.edges, &edge{
			label:  fragment,
			target: newNode(1, word, nil)})
		break
	}
}

type walkFunc func(*Node)

func (t *tree) Walk(fn walkFunc) {
	recursiveWalk(t.root, fn)
}

func recursiveWalk(n *Node, fn walkFunc) {
	if !n.isRoot() {
		fn(n)
	}
	if n.isLeaf() {
		return
	}

	for _, e := range n.edges {
		recursiveWalk(e.target, fn)
	}
}

func matchingPrefix(l, r []byte) int {
	matchingLength := -1

	if len(l) == 0 || len(r) == 0 {
		return matchingLength
	}

	a, b := l, r
	if len(l) > len(r) {
		a, b = r, l
	}

	for k := range a {
		if a[k] != b[k] {
			break
		}
		matchingLength++
	}

	return matchingLength
}
